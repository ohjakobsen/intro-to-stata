# Importing data

## Demo *Stata commands*

1. Make sure there are no files in memory
2. Navigate to the `intro-to-stata` folder
3. Use `cd "data"` to set the `data`-folder as the working directory
4. Use the `pwd`-command to verify that the `data`-folder is now the working directory
5. Use the `ls`-command to list the files in the directory
6. Open the `x.dta` file using `use x.dta`
7. Open the `x.dta` file again using `use x.dta`. Note that this will give an error message
8. Open the `x.dta` file again using `use x.dta, clear`. Note that this will work

## Demo *Importing data to Stata*

Import the `boston.csv` data file:

```stata
import delim boston.csv, varn(1)
```

We can import a subset of the data. The following command will import the first 20 observations. Remember to use the command `clear`.

```stata
* Note that the first row is the header
import delim boston.csv, varn(1) rowr(1:21)
```

Look at `help import` for more information. Click on `import delimited`.

## Assignment

* How many variables are in the file?
* What kind of variable are the variables price and type?
* How many unique values do the variable manufacturer have?

**Answers:**

```stata
* 27 variables
* Can be seen from the import window, but also use the following commands:
describe

* price is numeric (float) and type is string (str7)
* 32
codebook manufacturer, compact
```