# Graphics in Stata

## Demo *Graphics in Stata*

```stata
graph bar price, over(rep78) stack
histogram weight
histogram weight, bin(10)
```


## Demo *Twoway graphs*

```stata
sysuse auto, clear
```

Run the following commands:

```stata
graph twoway scatter mpg weight
twoway scatter mpg weight
scatter mpg weight
```

The `()`-binding notation is easier to read

```stata
twoway scatter mpg weight, by(foreign)
twoway scatter mpg weight || lfit mpg weight
twoway (scatter mpg weight) (lfit mpg weight)
twoway (qfitci mpg weight, stdf) (scatter mpg weight), by(foreign)
```