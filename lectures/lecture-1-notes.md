# Introduction to Stata

## Demo *Stata basics*

```stata
help data
```

This will bring up the help system in Stata, and go to the help pages for **data mangagement**. Click on the command `use` under the list of commands.

Close the window. In the command window, write:

```stata
search commands
```

Click on `gs` and then on `GSW` under **Introducing Stata**.

## Demo *Look at data*

Write the following command:

```stata
browse
```

Red text tells you that you have a text, also called *string*, variable. Blue text tells you that you have a numeric variable with **labels**. Close the window and write the following command:

```stata
browse, nolabel
```
Notice that the `foreign`-variable is now black and with the values `0` and `1`. Close the window and write the following command:

```stata
edit
```

Notice that we can now write values to the cells. Change the second value of `mpg` to `18`, then change it back to `17`. Close the window. Note that Stata has written both the *command* to perform the changes, and how many observations that have been changed (1).

## Demo *Using commands*

```stata
describe price
d price
d p
d m // this will result in an error
d m*
```

## Demo *The display command*

```stata
display 2 * 2
di 2 * 2
di 2 * (2 + 2)
```

Short assignment

* What is 46 minus 16 divied by 4?
* What is 4 to the power of 4?
* What is 56 divided by the product of 4 and 3?
* What is 24 multiplied with 3 to the power of 2?

**Answers:**

```stata
di 46 - (16 / 4)
di 4^4
di 56 / (4 * 3)
di 24 * 3^2
```

## Answers to assignment

* What is the 24th observation of the variable `price`?
* What is the values on the variable `price` for the five last observations in the data?
* Use the `summarize` command: what is the mean price for car models with a repair record less than 3?
* Use the `bysort` qualifier with the `summarize` command: which car type (the `foreign` variable) has the highest mean price?

**Answers:**

```stata
sysuse auto, clear
list price in 24
display price[24] // alternative solution
list price in -5/l
summarize price if rep78 < 3
bysort foreign: summarize price
```