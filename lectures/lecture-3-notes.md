# Descriptive statistics

## Demo *tabstat command*

View the help file for the `tabstat` command with `help tabstat`. Go down to the `statistics` option and click on `statname`. Discuss the various statistics that are available with the `tabstat` command.

Run the following commands and explain the output.

```stata
tabstat mpg
tabstat mpg, s(p50 mean n)
tabstat mpg, s(min max count)
tabstat mpg, s(mean) by(rep78)
```

## Assingment

* What is the average rpm of the car models?
* What is the median rpm of the car models?
* What is the most frequent airbags set-up?
* What is the least frequent type of car?
* Which car type has the highest median price?

**Answers:**

```stata
* Opening the file
import delim cars.csv, varn(1) clear

* Answers
su rpm // 5281
su rpm, det // 5200
tab airbags // Driver only
tab type // Van
tabstat price, s(p50) by(type) // Midsize
```

## Demo *missing data*

## Assignment

* Summarise the missing values for this file (which variables have missing values, and how many)
* List the car models that have missing on the variable `luggageroom`
* Generate a new variable `largetrunk` that is equal to 1 if `luggageroom` is greater than 15 and 0 if not. Missing values on `luggageroom` should be missing on `largetrunk`

**Answers:**

```stata
import delim cars.csv, clear
misstable sum
list model if luggageroom == .
gen largetrunk = luggageroom > 15 if luggageroom != .
```