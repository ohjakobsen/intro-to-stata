# Final assignment

Suggested solution:

Open the file with the following command (remember to set your working directory first):

```stata
import excel "titanic.xlsx", first clear
```

## Part 1

* Replace incorrect values in `Pclass` and destring the variable to numeric
* Encode a numeric variable `female` from the `Sex` variable with 0 = male and 1 = female. Add value labels
* Destring the variable `Age`
* Replace values in `Embarked` with full name of the port of embarkation
* Encode the `Embarked` variable into a categorical variable in numeric format and value labels
* Generate a binned (categorical) variable for `Fare`

The `Pclass` variable include two values that are incorrect. We see this by tabluating `Pclass` with `tab Pclass`. Note the values `1+` and `h2`. We want to replace these values in order to destring the variable later (if we destring now, we will get missing values). We replace the two incorrect values with valid values and then destring the variable. We use the options `replace` and `force` to replace the character values in `Pclass` with numerical values.

```stata
replace Pclass = "1" if Pclass == "1+"
replace Pclass = "2" if Pclass == "h2"
destring Pclass, replace force
```

We can use the `encode` command to generate a new variable with numeric values. If we first specify the labels, Stata will map the old character values to our new numerical values.

```stata
label define female 0 "male" 1 "female"
encode Sex, generate(female) label(female)
```

By looking at the variable `Age` with `tab Age`, se see that it has been imported as character because missing values are coded with `-`. The rest of the values are numeric with `.` as the decimal point (the same that Stata uses). We can therefore simply use the `destring` command.

```stata
destring Age, replace force
```

From `tab Embarked`, we see that the variable `Embarked` has the values `C`, `Q` and `S`. We want to first replace these values with the full names of the ports, and than encode the variable into a new numerical variable with value labels.

```stata
replace Embarked = "Cherbourg" if Embarked == "C"
replace Embarked = "Queenstown" if Embarked == "Q"
replace Embarked = "Southampton" if Embarked == "S"
label define embark 1 "Cherbourg" 2 "Queenstown" 3 "Southampton"
encode Embarked, gen(embark) label(embark)
```

We can verify that we get the correct result:

```stata
tab embark
```

The `egen` command has a function called `cut` that can be used to create binned (categorical) variables from continous variables. There are several ways to do this, and the assignment does not specify any specific result. Here, we use the `cut` function to create four - 4 - equally sized groups from the `Fare` variable.

```stata
egen fareCat = cut(Fare), group(4)
```

## Part 2

* Cross-tabulate passenger class and port of embarkment. Include row totals
* Tabulate the survival rate for each passenger class
* Tabulate the survival rate for each port of embarkation
* Print out (using a command of your choice) the average ticket price for survival and non-survival
* Print out (using a command of your choice) the average and median age for survival and non-survival

We use the `tabulate` command to cross-tabulate passenger class and port of embarkment. By using the option `row`, we get row totals.

```stata
tab Pclass Embarked, row
```

There are several ways to get the survival rate (the proportion of passengers with `Survived == 1`). The simplest option would be to use a cross-tabulation with row totals. We can also do a single tabulation with the `bysort` qualifier. Stata also have a command for estimating proportions. All of these commands should give the same result:

```Stata
tab Pclass Survived, row
bysort Pclass: tab Survived
proportion Survived, over(Pclass)
```

We do the same for the `embark` variable created in the first part of the assignment:

```Stata
tab embark Survived, row
bysort embark: tab Survived
proportion Survived, over(embark)
```

We can get detailed statistics for a variable with the `tabstat` command. We specify the statistics we want with the `s` option. We can use the `by` option to run the command over a categorical variable, in this case `Survived`. When we only want the mean, an alternative is to use the `mean` command. This will give us confidence intervals for the mean estimation, in addition to the mean, which can be useful for samples. Note that the `mean` command use the option `over` and not `by`.

```Stata
tabstat Fare, s(mean) by(Survived)
mean Fare, over(Survived)
```

To get both the mean and the median, we use the `tabstat` command.

```Stata
tabstat Age, s(mean p50) by(Survived)
```