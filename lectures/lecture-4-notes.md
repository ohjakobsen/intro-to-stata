# Data manipulation

## Demo *generate command*

```stata
* Generate the square of mpg
gen mpgeSq = mpg^2

* Generate a binary variable
gen highprice = price >= 10000

* Check the variable
tab highprice
tabstat price, s(min max) by(highprice)
```

Other commands can also be used to generate new variables. The `tabulate` command has an option to generate new variables. This is really useful if we want to create dummy variables:

```stata
tab rep78, g(repd)
```

## Assingment

```stata
sysuse auto, clear
gen mpgSq = mpg^2
su mpg mpgSq
su mp*
gen highprice = price >= 10000
tab highprice
tabstat price, s(min max) by(highprice)
```

## Demo *functions*

Use functions with the `display` command_

```stata
di runiform()
di log(4)
di sqrt(16)
di strlen("word")
```

Now we create a data set and add variables with random numbers:

```stata
clear
set obs 5
gen random = runiform()
br
gen random2 = rnormal(10, 2)
br
```

Load the auto file. We will now create new variables using functions in our expressions.

```stata
* Go back to auto file
sysuse auto, clear

* Use mathematical functions
gen priceLog = log(price)
gen priceSqrt = sqrt(price)

* Create variables for make and model
gen carmake = word(make, 1)
gen carmodel = word(make, 2) + word(make, 3)
br make carm*
```

## Demo *System variables*

The system variables `_n` and `_N` are powerful tools.

```stata
sysuse auto, clear
di mpg[5]    // show the fifth value
di mpg[_N]   // show the last value
di mpg[_N-1] // show the next to last value
```

We can use these variables together with the `generate` command:

```stata
gen lag_mpg = mpg[_n-1]
gen lead_mpg = mpg[_n+1]

* View the result
br mpg lag_mpg lead_mpg

* Create an ID
gen ID = _n

* Replace (change) the values in ID
replace ID = ID + 100

* Another example
bysort foreign: gen type_n = _N

* Look at the result
tab type_n
```

## Assignment

* Generate a new variable `murderrate` with the ratio of murders to assaults for each state
* Generate a new variable `highrate` if the murder rate is equal to or greater than .08
* Generate a new variable `crime` that adds the number of murders, assaults and rapes for each state
* Generate a new variable `crimeSq` with the squared number of crimes

**Answers:**

```stata
gen murderrate = murder / assault
gen highrate = murderrate >= .08
gen crime = murder + assault + rape
gen crimeSq = crime^2
```

## Demo *Replace and recode*

We will first cover some basic usage of `recode`:

```stata
recode rep78 (1 = 1) (2 3 = 2) (4 5 = 3), gen(rep78n)
```

Let's look at the `mpg` variable and recode it into a categorical variable:

```stata
ins mpg
recode mpg (12/20 = 1) (21/30 = 2) (31/41 = 3), gen(mpgc)
```

We look at our new variable with `tab mpgc`.

```stata
* Does the same, but we don't need to remember the min and max
recode mpg (min/20 = 1) (21/30 = 2) (31/max = 3), gen(mpgc2)
```

Create a new variable with 0 or 1 if `rep78` is missing:

```stata
recode rep78 (nonmiss = 1) (miss = 0), gen(repmiss)
tab rep78 repmiss, mi
```

Another way to to the same, would be to use `generate` and then `replace`:

```stata
gen repmiss2 = 0 if rep78 >= .
replace repmiss2 = 1 if rep78 < .
```

## Demo *Working with strings*

Look up the help file for string functions by typing `help string functions` in the command window.

```stata
di strlen("UPPERCASE")
di strlower("UPPERCASE")
```

We open the `datacleaning.dta` file. First we start with the `gender` variable. We inspect the variable by running a frequency table: `tab gender`

```stata
recast str1 gender, force
replace gender = "Female" if gender == "W" | gender == "F"
replace gender = "Male" if gender == "O" | gender == "M"
label define sex 0 "Male" 1 "Female"
encode gender, generate(female) label(sex)
```

Notice that I've named the new variable `female` and not `sex` or `gender`. This is because it is a lot easier to guess the meaning of the value `1` when the variable is named `female`. We now convert `yrsemp` to numeric format:

```stata
destring yrsemp, replace force
```

And finally we convert `empstatus` to a numeric variable:

```stata
replace empstatus = "No" if empstatus == "N"
replace empstatus = "Yes" if empstatus == "Y"
replace empstatus = "Retired" if empstatus == "R"
label define employed 1 "Yes" 2 "No" 3 "Retired"
encode empstatus, generate(employed) label(employed)
```

## Assignment

* Convert id from a string to a numeric variable
* Encode the variable sex into a new variable female where the value 0 equals male and the value 1 equals female.  The new variable should have value labels

**Answers:**

```stata
destring id, replace force
label define female 0 "male" 1 "female"
encode sex, generate(female) label(female)
```

## Demo *appending files*

Make sure you are in the `data` folder in the `intro-to-stata` directory, and that the files `domestic.dta` and `foreign.dta` are available.

1. Clear memory with the `clear` command
2. List the files with the `ls` command
3. Open the `domestic.dta` file with `use domestic`
4. Append the `foreign.dta` file

```stata
append using foregin
```

Look at the merged data file with the `browse` command.

## Demo *merging files*

1. Clear memory with the `clear` command
2. List the files with the `ls` command
3. Open the `autosize.dta` file with `use autosize`
4. Merge the `autoexpense.dta` file

```stata
merge 1:1 make using autoexpense
```

Use `list` to list the results of the merged file. Clear memory, and start over, but use this merge command:

```stata
merge 1:1 make using autoexpense, keep(match)
```

List the results. If we want to specify, what to keep, we can add the values for the `_merge` variable in our keep option. This will keep results from master and match, but not using:

```stata
merge 1:1 make using autoexpense, keep(1 3)
```

## Assingment

* Append the coffeeMaize2 data set to the coffeeMaize data set. Generate a variable to indicate which data set the observation is from
* Merge the ind2 data set (using) with the hh2 data set (master) with hid as the key

**Answers:**

```stata
// add after course
```