# Monday October 16

* *09:00-10:00* Introduction to Stata
* *10:00-10:30* Importing data
* *10:30-10:50* **Lab:** Import data from various sources 
* *10:50-11:10* Break 
* *11:10-12:20* Descriptive statistics 
* *12:20-13:00* **Lab:** Descriptive statistics 
* *13:00-14:00* Lunch 
* *14:00-15:00* Data manipulation 
* *15:00-16:00* **Lab:** Data manipulation 

# Tuesday October 17

* *09:00-10:00* Data manipulation: part II 
* *10:00-11:00* **Lab:** 
* *10:50-11:10* Break 
* *11:10-12:00* Graphics in Stata 
* *12:00-13:00* **Lab:** Graphics in Stata
* *13:00-14:00* Lunch 
* *14:00-14:40* Documenting the analysis 
* *14:40-16:00* **Lab:** Analysis and documentation 

# Outline of lectures

## Introduction to Stata

* Get to know the user interface
* Commands and do files
* Command structure
* Entering data
* Importing data
* Saving data

## Descriptive statistics

* Data types in Stata
* Frequency tables
* Two-way tables
* Summary statistics
* Tabstat functions
* Missing data

*Extra material for advanced users*

* Regression analysis
* Diagnostic plots

## Data manipulation

* Generate new variables
* Operators in Stata
* Variable and value labels
* Recode variables
* Recast variables
* String manipulation

## Data manipulation: part II

* Append data
* Merge data
* Reshape data
* Collapse data

## Documenting the analysis

* Documenting do files
* Using log files

# Assignments

* use the display command
* get information from the auto filie
* import the cars.csv file
* descriptive statistics cars.csv file
* missing data on the cars.csv file


# Demo for management

Open Stata

```stata
sysuse auto
````

Fit a regression model

```stata
regress mpg weight foreign
```

Generate a new variable, and fit a new regression:

```stata
generate gp100m = 100/mpg
regress gp100m weight foreign
```

Get the beta coefficients

```stata
regress, beta
```
