/*
 * The data files are also available on GitLab
 * use "webuse set https://gitlab.com/ohjakobsen/intro-to-stata/raw/master/data"
 * in your command window to set the webuse command to the GitLab directory.
 * You can then use "webuse autosize, clear" to load the data file.
 * Use "webuse set" to return to the default webuse directory.
 */

* Open the autosize file
use autosize, clear

* Merge the autosize file with autoexpense
* We only keep the observations that have a match
merge 1:1 make using autoexpense, keep(3)

* We list the resulting merged file
list
