```stata
. list

     +---------------------------+
     | var1   var2   var3   var4 |
     |---------------------------|
  1. |    1      1     30      4 |
  2. |    2      2      .      6 |
  3. |    3      1     20      0 |
  4. |    4      3     10      8 |
  5. |    5      1     15      7 |
     |---------------------------|
  6. |    6      2      .      9 |
     +---------------------------+

. label variable var1 "My variable"

. label variable var2 "Another variable"

. la var var3 "A thrid variable"

. la var var4 "My last variable"

. tab var2

    Another |
   variable |      Freq.     Percent        Cum.
------------+-----------------------------------
          1 |          3       50.00       50.00
          2 |          2       33.33       83.33
          3 |          1       16.67      100.00
------------+-----------------------------------
      Total |          6      100.00

. label define alabel 1 "Yes" 2 "No" 3 "Don't know"

. label value var2 alabel

. tab var2

    Another |
   variable |      Freq.     Percent        Cum.
------------+-----------------------------------
        Yes |          3       50.00       50.00
         No |          2       33.33       83.33
 Don't know |          1       16.67      100.00
------------+-----------------------------------
      Total |          6      100.00

. browse

. list

     +---------------------------------+
     | var1         var2   var3   var4 |
     |---------------------------------|
  1. |    1          Yes     30      4 |
  2. |    2           No      .      6 |
  3. |    3          Yes     20      0 |
  4. |    4   Don't know     10      8 |
  5. |    5          Yes     15      7 |
     |---------------------------------|
  6. |    6           No      .      9 |
     +---------------------------------+
```