# Extra assignment

1. Open Stata and load the auto file
2. Look at the help entry for the `regress` command
3. Fit a regression model with the `mpg` variable as the dependent variable, and `weight` and `foregin` as independent variables
4. Explain the results of the regression
5. Generate a new variable called `gp100m` that is equal to `100 / mpg`
6. Fit a new regression model with `gp100m` as the dependent variable, and `weight` and `foregin` as independent variables
7. Get the beta coefficients for the model (hint: look at the options for the `regress` command)
8. Explain the results of the regression
