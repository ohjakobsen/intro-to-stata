---
title: "Solution to first lab"
author: "Ove Haugland Jakobsen"
date: "October 16, 2017"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, eval = FALSE)
```

## First lab

```{stata}
gen newvar = oldvar * 1
```

