# Read me, or else

All data files should go in the `data` directory

All do files should go in the `do` directory

All assingments and solutions to assingments should go in the `labs` directory and should be in R Markdown (for generating PDFs) or Markdown

All documentation should go in the `docs` directory

All presentations and files supporting them should go in the `lectures` directory

Everything else should go in the `misc` directory

Be happy!